import utils

import collections


# only list pairs with Jaro-Winkler ratio greater than this
MIN_RATIO = 0.85



def main():
	by_ratio = collections.defaultdict(list)
	with open("words.txt") as f:
		#print("forming pairs...")
		magnitude = 2
		i = 0
		single_letters = set()
		# first two letters currently looked at
		initials = ""
		kept = []
		for line in f:
			word, frequency = line.split(" ", 1)
			# ignore single-instance words
			if int(frequency) < 2:
				continue
			if len(word) < 2:
				single_letters.add(word)
				continue
			elif word[0:2] != initials:
				kept.clear()
				initials = word[0:2]
			else:
				for other_word in kept:
					ratio = utils.calculate_ratio(word, other_word)
					if ratio > MIN_RATIO:
						by_ratio[ratio].append((other_word, word))
						i += 1
				if word[0] in single_letters:
					ratio = utils.calculate_ratio(word, word[0])
					if ratio > MIN_RATIO:
						by_ratio[ratio].append((word[0], word))
						i += 1
				kept.append(word)
				if i > 10**magnitude:
					print(10**magnitude)
					magnitude += 1

	print("sorting...")
	order = sorted(by_ratio)

	with open("pairs.txt", "w") as f_out:
		print("writing to file...")
		while len(order) > 0:
			ratio = order.pop()
			items = by_ratio[ratio]
			while len(items) > 0:
				word_a, word_b = items.pop()
				f_out.write("%s %s %s\n" % (ratio, word_a, word_b))




if __name__ == "__main__":
    main()