import utils


FREQ_INFLUENCE = 1 / 1000
MIN_SCORE = 0.97


def main():
    freqs = {}
    with open("words.txt") as f_words:
        for line in f_words:
            word, count = line.split()
            freqs[word] = int(count)

    word_sets = {}
    remaining_names = set()
    with open("split_sets.txt", "r") as f_sets:
        for line in f_sets:
            words = line.split()
            name = words[0][:-1]
            word_sets[name] = set(words[1:])
            remaining_names.add(name)

    print("finding sets to combine")
    sets_to_combine = []
    for word_set_name in word_sets:
        if word_set_name[0] == "%":
            continue
        for other_set_name, other_set_words in word_sets.items():
            if other_set_name == word_set_name:
                continue
            if other_set_name[:2] != word_set_name[:2]:
                if other_set_name[0] == "%" and other_set_name[1:3] == word_set_name[:2]:
                    pass
                else:
                    continue
            best = None
            for word in other_set_words:
                freq_multi = (freqs[word_set_name] ** FREQ_INFLUENCE) * (freqs[word] ** FREQ_INFLUENCE)
                score = utils.calculate_ratio(word_set_name, word) * freq_multi
                if best is None or score > best[0]:
                    best = (score, word)
            if best is not None and best[0] >= MIN_SCORE:
                sets_to_combine.append((best[0], word_set_name, other_set_name, best[1]))
                if len(sets_to_combine) % 200 == 0:
                    print(len(sets_to_combine))

    with open("sets_to_combine.txt", "w") as f_out:
        for pair in sets_to_combine:
            line = "%s %s %s %s\n" % (pair)
            f_out.write(line)




if __name__ == '__main__':
    main()