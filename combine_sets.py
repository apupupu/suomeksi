

def main():
    freqs = {}
    with open("words.txt") as f_words:
        for line in f_words:
            word, count = line.split()
            freqs[word] = int(count)

    word_sets = {}
    remaining_names = set()
    with open("split_sets.txt", "r") as f_sets:
        for line in f_sets:
            words = line.split()
            name = words[0][:-1]
            word_sets[name] = set(words[1:])
            remaining_names.add(name)

    sets_to_combine = []
    with open("sets_to_combine.txt", "r") as f:
        for line in f:
            words = line.split()
            sets_to_combine.append((words[1], words[2]))

    new_sets = {}
    for name_a, name_b in sets_to_combine:
        if new_sets.get(name_a) is None:
            if new_sets.get(name_b) is None:
                new = {name_a, name_b}
                new_sets[name_a] = new
                new_sets[name_b] = new
            else:
                new_sets[name_b].add(name_a)
                new_sets[name_a] = new_sets[name_b]
        else:
            new_sets[name_a].add(name_b)
            new_sets[name_b] = new_sets[name_a]

    print("writing combined_sets.txt")
    with open("combined_sets.txt", "w") as f_out:
        while len(remaining_names) > 0:
            best_name = remaining_names.pop()
            best_name_freq = 0
            if best_name in new_sets:
                name_set = new_sets[best_name]
                #new_sets.remove(best_name)
                word_set = set()
                for name in name_set:
                    if name in remaining_names:
                        remaining_names.remove(name)
                    freq = freqs[name]
                    word_set |= word_sets[name]
                    if name in new_sets:
                        del new_sets[name]
                    if freq > best_name_freq:
                        best_name = name
                        best_name_freq = freq
            else:
                word_set = word_sets[best_name]

            line = "%s: %s\n" % (best_name, " ".join(word_set))
            f_out.write(line)




if __name__ == '__main__':
    main()