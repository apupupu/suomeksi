


def main():
	word_freqs = {}
	with open("words.txt") as words_f:
		for line in words_f:
			word, count = line.split()
			word_freqs[word] = int(count)

	# name: dict
	dicts = {}
	with open("combined_sets.txt") as sets_f:
		for line in sets_f:
			words = line.split()
			word_dict = {}
			sum_ = 0
			for word in words[1:]:
				count = word_freqs[word]
				word_dict[word] = count
				sum_ += count
			#name = max(word_dict, key=word_dict.get).upper()
			ordered = sorted(word_dict.items(), key=lambda item: item[1], reverse=True)
			name = words[0][:-1].upper()
			if name[0] == "%":
				name = ordered[0][0].upper()
			ordered.append((name, sum_))
			dicts[name] = ordered

	group_order = sorted(dicts, key=lambda name: dicts[name][-1][1], reverse=True)

	with open("word_groups.txt", "w") as f_out:
		for name in group_order:
			group = dicts[name]
			f_out.write("%s %i\n" % (name, group.pop()[1]))
			for word, freq in group:
				f_out.write("\t%s %i\n" % (word, freq))
			f_out.write("\n")




if __name__ == "__main__":
    main()