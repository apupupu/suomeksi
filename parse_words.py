import csv
import collections

LETTERS = "qwertyuiopåasdfghjklöäzxcvbnmš"
WORD_CHARS = LETTERS + "-'"
BANNED_STARTS = ("http")
MAX_COMBO = 2

def main():
    freqs = collections.defaultdict(int)
    with open("suomeksi.csv") as f:
        reader = csv.reader(f, delimiter=";")
        for line in reader:
            msg = line[3].split();
            for word in msg:
                lower = word.lower()
                letters = []
                # words must start and end on letters
                started = False
                emoji = False
                prev = ""
                combo = 0
                for i, char in enumerate(lower):
                    # too many of any one character in a row not allowed
                    if prev == char:
                        combo += 1
                    else:
                        combo = 0
                        prev = char
                    if combo >= MAX_COMBO:
                        break

                    if char == ":":
                        emoji = not emoji
                    elif emoji:
                        continue
                    elif started and char in WORD_CHARS:
                        letters += char
                    elif char in LETTERS:
                        letters += char
                        started = True
                    # other characters split words
                    elif started:
                        msg.append(lower[i+1:])
                        break
                for char in reversed(letters):
                    if char in LETTERS:
                        break
                    else:
                        letters.pop()
                clean_word = "".join(letters)
                if clean_word.startswith(BANNED_STARTS):
                    continue
                if len(clean_word) > 0:
                    freqs[clean_word] += 1

    words = sorted(freqs)

    with open("words.txt", "w") as f_out:
        for word in words:
            f_out.write("%s %i\n" % (word, freqs[word]))


if __name__ == "__main__":
    main()