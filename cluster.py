import collections


MIN_RATIO = 0.94
MAX_SIZE = 500


class WordSets():
	def __init__(self):
		self.sets_by_initials = collections.defaultdict(list)

	def add_pair(self, word_a, word_b):
		initials = word_a[0:2]
		assert initials == word_b[0:2]
		other_set = None
		# important: a word may never belong to more than one set
		for i, set_ in enumerate(self.sets_by_initials[initials]):
			if word_a in set_ or word_b in set_:
				# extend existing set of words
				if other_set is None:
					other_set = set_
					if len(set_) < MAX_SIZE:
						set_.add(word_a)
						set_.add(word_b)
					else:
						break
				# combine two sets of words
				else:
					if len(set_) + len(other_set) > MAX_SIZE:
						#print("Warning: Combining %s and %s exceeds threshold" % (word_a, word_b))
						#break
						pass
					other_set |= set_
					del self.sets_by_initials[initials][i]
					break
		# add new set of words
		if other_set is None:
			self.sets_by_initials[initials].append({word_a, word_b})

	def __iter__(self):
		order = sorted(self.sets_by_initials, reverse=True)
		while len(order) > 0:
			initials = order.pop()
			for set_ in self.sets_by_initials[initials]:
				yield set_



def main():
	word_sets = WordSets()
	with open("pairs.txt") as f:
		for line in f:
			ratio, word_a, word_b = line.split()
			if float(ratio) <= MIN_RATIO:
				break
			word_sets.add_pair(word_a, word_b)

	largest = 0
	with open("sets.txt", "w") as f_out:
		for word_set in word_sets:
			f_out.write("%s\n" % " ".join(word_set))
			if len(word_set) > largest:
				largest = len(word_set)

	print("Largest word set has %s items" % largest)


if __name__ == "__main__":
    main()