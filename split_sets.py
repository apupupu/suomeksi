import utils

import collections
import queue


FREQ_INFLUENCE = 1 / 1000
MIN_SCORE = 0.95


def main():
    wordlist = set()
    with open("wordlist.txt", "r") as f_wl:
        for line in f_wl:
            wordlist.add(line[0:-1])

    freqs = {}
    with open("words.txt") as f:
        for line in f:
            word, freq = line.split()
            freqs[word] = int(freq)

    print("splitting sets")
    new_sets = {}
    with open("sets.txt", "r") as f_in:
        i = 0
        for line in f_in:
            i += 1
            if i % 100 == 0:
                print(i)

            unclassified_words = set()
            word_sets = {}
            all_set_words = []

            for word in line.split():
                all_set_words.append(word)
                if word in wordlist:
                    word_sets[word] = {word}
                else:
                    unclassified_words.add(word)

            all_set_scores = collections.defaultdict(list)
            for word_a in all_set_words:
                for word_b in all_set_words:
                    if word_a == word_b:
                        continue
                    freq_multi = (freqs[word_a] ** FREQ_INFLUENCE) * (freqs[word_b] ** FREQ_INFLUENCE)
                    score = utils.calculate_ratio(word_a, word_b) * freq_multi
                    if score >= MIN_SCORE:
                        all_set_scores[word_a].append((score, word_b))
                        all_set_scores[word_b].append((score, word_a))

            # populate queue with all the connections to those words
            active_scores = queue.PriorityQueue()
            for word in word_sets:
                for item in all_set_scores[word]:
                    # (negative_score, word, set_name)
                    active_scores.put((-item[0], item[1], word))

            while not active_scores.empty():
                while True:
                    try:
                        neg_score, word, set_name = active_scores.get_nowait()
                    except queue.Empty:
                        word = None
                        break
                    if word in unclassified_words:
                        break
                if word is not None:
                    unclassified_words.remove(word)
                    word_sets[set_name].add(word)
                    for item in all_set_scores[word]:
                        active_scores.put((-item[0], item[1], set_name))


            if len(unclassified_words) > 0:
                word_sets["%%%s" % "_".join(unclassified_words)] = unclassified_words

            for name, new_set in word_sets.items():
                new_sets[name] = new_set


    print("writing split_sets.txt")
    with open("split_sets.txt", "w") as f_out:
        for name, word_set in new_sets.items():
            line = "%s: %s\n" % (name, " ".join(word_set))
            f_out.write(line)




if __name__ == "__main__":
    main()