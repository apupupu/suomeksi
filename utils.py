
# https://pypi.org/project/python-Levenshtein/
import Levenshtein


def calculate_ratio(word_a, word_b):
	#min_len = min(len(word_a), len(word_b))
	max_len = max(len(word_a), len(word_b))
	return Levenshtein.jaro_winkler(word_a, word_b, 1 / (max_len + 1))
