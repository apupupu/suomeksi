
def main():
    with open("kotus-sanalista_v1/kotus-sanalista_v1.xml", "r") as f_in:
        with open("wordlist.txt", "w") as f_out:
            for line in f_in:
                if len(line) < 7 or line[0:7] != "<st><s>":
                    continue
                end = line.find("<", 7)
                word = line[7:end]
                f_out.write("%s\n" % word)



if __name__ == "__main__":
    main()